package books.search.book.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import books.search.book.entities.PublishedDate;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"title",
"isbn",
"pageCount",
"publishedDate",
"thumbnailUrl",
"shortDescription",
"longDescription",
"status"
})
@Entity
public class Book {


@Id
@GenericGenerator(name="kaugen" , strategy="increment")
@GeneratedValue(generator="kaugen")
private long id;
@JsonProperty("title")
private String title;
@JsonProperty("isbn")
@Column(nullable=false)
private String isbn = "NO-ISBN";
@JsonProperty("pageCount")
private Integer pageCount;
@JsonProperty("publishedDate")
@Transient
private PublishedDate publishedDate;
@JsonProperty("thumbnailUrl")
private String thumbnailUrl;
@Transient
@Lob
@Column(length = 10000, name="shortDescription")
@JsonProperty("shortDescription")
private String shortDescription;
@Transient
@JsonProperty("longDescription")
private String longDescription;
@JsonProperty("status")
private String status;
@Column(nullable=true)
@Transient
@JsonProperty("authors")
@ElementCollection(targetClass=String.class)
public List<String> authors = null;
@Column(nullable=true)
@Transient
@JsonProperty("categories")
@ElementCollection(targetClass=String.class)
public List<String> categories = null;

public void setid(long id) {
	this.id = id;
}
public long getid() {
	return id;
}


@JsonProperty("title")
public String getTitle() {
return title;
}

@JsonProperty("title")
public void setTitle(String title) {
this.title = title;
}

@JsonProperty("isbn")
public String getIsbn() {
return isbn;
}

@JsonProperty("isbn")
public void setIsbn(String isbn) {
this.isbn = isbn;
}

@JsonProperty("pageCount")
public Integer getPageCount() {
return pageCount;
}

@JsonProperty("pageCount")
public void setPageCount(Integer pageCount) {
this.pageCount = pageCount;
}

@JsonProperty("publishedDate")
public PublishedDate getPublishedDate() {
return publishedDate;
}

@JsonProperty("publishedDate")
public void setPublishedDate(PublishedDate publishedDate) {
this.publishedDate = publishedDate;
}

@JsonProperty("thumbnailUrl")
public String getThumbnailUrl() {
return thumbnailUrl;
}

@JsonProperty("thumbnailUrl")
public void setThumbnailUrl(String thumbnailUrl) {
this.thumbnailUrl = thumbnailUrl;
}

@JsonProperty("shortDescription")
public String getShortDescription() {
return shortDescription;
}

@JsonProperty("shortDescription")
public void setShortDescription(String shortDescription) {
this.shortDescription = shortDescription;
}

@JsonProperty("longDescription")
public String getLongDescription() {
return longDescription;
}

@JsonProperty("longDescription")
public void setLongDescription(String longDescription) {
this.longDescription = longDescription;
}

@JsonProperty("status")
public String getStatus() {
return status;
}

@JsonProperty("status")
public void setStatus(String status) {
this.status = status;
}


}


