package books.search.book.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"date"})

@Entity
public class PublishedDate {

@javax.persistence.Id
@GeneratedValue(strategy=GenerationType.AUTO)
private int Id;


@JsonProperty("date")
private String date;


@JsonProperty("date")
public String getdate() {
return date;
}

@JsonProperty("date")
public void setdate(String date) {
this.date = date;
}


}