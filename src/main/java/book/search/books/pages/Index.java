package book.search.books.pages;


import org.apache.tapestry5.Block;
import org.apache.tapestry5.EventContext;
import org.apache.tapestry5.SymbolConstants;
import org.apache.tapestry5.annotations.InjectComponent;
import org.apache.tapestry5.annotations.InjectPage;
import org.apache.tapestry5.annotations.Log;
import org.apache.tapestry5.annotations.Property;
import org.apache.tapestry5.annotations.SetupRender;
import org.apache.tapestry5.corelib.components.Form;
import org.apache.tapestry5.corelib.components.TextField;
import org.apache.tapestry5.hibernate.annotations.CommitAfter;
import org.apache.tapestry5.ioc.annotations.Inject;
import org.apache.tapestry5.ioc.annotations.Symbol;
import org.apache.tapestry5.services.HttpError;
import org.apache.tapestry5.services.ajax.AjaxResponseRenderer;
import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.slf4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.introspect.VisibilityChecker;

import books.search.book.entities.Book;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nonnull;

/**
 * Start page of application books.
 */
public class Index
{
  @Inject
  private Logger logger;

  @Inject
  private AjaxResponseRenderer ajaxResponseRenderer;

  @Property
  @Inject
  @Symbol(SymbolConstants.TAPESTRY_VERSION)
  private String tapestryVersion;

  @InjectPage
  private About about;

  @Inject
  private Block BooksList;
  
  
  @Inject
  private Block NoRecords;
  
  
  
  @InjectComponent("ajaxForm")
  private Form form;
  
  
  @InjectComponent("Query")
  private TextField QueryField;
  
  
  @Property
  private String Query;
  
  
  @Inject
  private Session session;
  @CommitAfter
  public void onSuccess() {
	  
  }
  
  @Property
  private ArrayList<Book> Books;
  
  @Property
  private Book book;
  
  @Property 
  private int TotalRecords = 0;
  
  @SetupRender
  @Log
  @CommitAfter
  void Render() throws IOException {
	  
	  String hql = String.format("delete from Book");
	  org.hibernate.Query query = session.createQuery(hql);
	  query.executeUpdate();
	    
	  Books = new ArrayList<Book>();
	  logger.info("Rendering json");
	  ObjectMapper objectMapper = new ObjectMapper();
	  
	  ObjectMapper mapper = new ObjectMapper();
	  mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
	  String FileDefaultPath = FileSystems.getDefault().getPath("Books.json").toAbsolutePath().toString();
	  Book[] BooksFromDB = objectMapper.readValue(new File(FileDefaultPath), Book[].class);
	  TotalRecords = BooksFromDB.length;
	  for(int i=0; i<BooksFromDB.length;i++) {
		  session.persist(BooksFromDB[i]);
		  Books.add(BooksFromDB[i]);
	  }
	  
	  logger.info(FileDefaultPath);
  }
  
  // Handle call with an unwanted context
  Object onActivate(EventContext eventContext)
  {
    return eventContext.getCount() > 0 ?
        new HttpError(404, "Resource not found") :
        null;
  }

  @Log
  void onValidateFromAjaxForm() {

      if (Query == null) {
          form.recordError(QueryField, "First Name must not be Acme.");
          logger.info("Empty Query");
      }else {
         Books = (ArrayList<Book>) session.createQuery("from Book where title like '%"+Query+"%' OR isbn like '%"+Query+"%'").list();
         
         if(Books.size()>0)
        	 ajaxResponseRenderer.addRender("BooksListRender", BooksList);
         else
        	 ajaxResponseRenderer.addRender("BooksListRender", NoRecords);
    	 
      }
      
  }

}
